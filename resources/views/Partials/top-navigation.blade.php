   
   <!-- Navigation -->
    <nav class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Start Bootstrap</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="/">Home</a>
                    </li>  
                    <li>
                        <a href="/myPost">My Post</a>
                    </li>
                    
                    <li>
                        <a href="/about">About</a>
                    </li>
                                       
                                       @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                     
                     @if (Auth::user()->type=='admin')
                       <li>
                        <a href="/imageUpload">Image Upload</a>
                    </li>  
                    <li>
                        <a href="/category">Category</a>
                    </li> 
                    <li>
                        <a href="">Edit User</a>
                    </li>
                    <li>
                        <a href="/deleteIndex">Delete Article</a>
                    </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            

                            <ul class="dropdown-menu" role="menu">
                                <li><a style="background-color:blue" href="{{ url('/profile') }}"><i class="fa fa-btn fa-user"></i>Profile</a></li>

                                <li><a style="background-color:green" href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                        @else
                    <li>
                        <a href="/imageUpload">Image Upload</a>
                    </li>  
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            

                            <ul class="dropdown-menu" role="menu">
                                <li><a style="background-color:blue" href="{{ url('/profile') }}"><i class="fa fa-btn fa-user"></i>Profile</a></li>

                                <li><a style="background-color:green" href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                    </li>
                     @endif
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>