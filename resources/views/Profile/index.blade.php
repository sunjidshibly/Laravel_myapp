@extends('Layouts.default')   



@section('Layouts.content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
             
                <br>
                  <table class="table table-striped table-bordered">
                   
                    <tr>
                        <span><td>Name</td></span>
                        <span><td>{{$profile->name}}</td></span>
                    </tr>
                    <tr>
                        <span><td>Email</td></span>
                        <span><td>{{$profile->email}}</td></span>
                    </tr>
                    <tr>
                        <span><td>Type</td></span>
                        <span><td>{{$profile->type}}</td></span>
                    </tr>
                    <tr>
                        <span><td>Profession</td></span>
                        <span><td>{{$profile->profession}}</td></span>
                    </tr>
                    <tr>
                        <span><td>Phone</td></span>
                        <span><td>{{$profile->phone}}
                    <tr>
                        <span><td>Address</td></span>
                        <span><td>{{$profile->address}}</td></span>
                    </tr>                
                </table>            
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
