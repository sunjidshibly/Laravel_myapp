@extends('Layouts.default')   



@section('Layouts.content')


    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">


 {!! Form::open(['route' => 'category.store', 'files'=> true]) !!}
                                    <div class="form-group">
                                        {!! Form::label('title', 'Title') !!}
                                        {!! Form::text('title', null, ['class' => 'form-control','required','autofocus']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('left', 'Left') !!}
                                        {!! Form::text('left', null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('right', 'Right') !!}
                                        {!! Form::text('right', null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('parent_id', 'Parent ID') !!}
                                        {!! Form::number('parent_id', null, ['class' => 'form-control']) !!}
                                    </div>
                                {!! Form::submit('Submit', array('class'=>'btn btn-primary')) !!}
                                {!! Form::reset('Reset', array('class'=>'btn btn-warning')) !!}

                                {!! Form::close() !!}
                                <br>


                                   <div>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>serial no</th>
                        <th>Parent ID</th>
                        <th>Title</th>
                        <th>Left</th>
                        <th>Right</th>
                        <th>Action</th>
                        
                    </tr>
                    </thead>


                    <tbody>
                    <?php $slNo = 0; ?>
                    @foreach($categories as $category)
                        <?php $slNo++ ?>
                    <tr>
                        <td>{!! $slNo !!}</td>
                        <td>{!! $category->parent_id !!}</td>
                        <td>{!! $category->title !!}</td>
                        <td>{!! $category->left !!}</td>
                        <td>{!! $category->right !!}</td>
                        <td> 
                        
                        <a href="{{url('/category/'.$category->id.'/edit')}}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"></i>Update</a>

                        {!! Form::open(array('method'=>'DELETE', 'route'=>array('category.destroy',$category->id)))!!}

                        {!! Form::submit('Delete', array('class'=>'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure want to Delete?");'))!!}
                                        {!! Form::close()!!}</td>
                    </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>

                
            </div>
        </div>
    </div>

    <hr>

   @endsection

