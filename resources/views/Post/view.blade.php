@extends('Layouts.default')   



@section('Layouts.content')
    <!-- Main Content -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-preview">
@foreach($viewPost->images as $postView) @endforeach

<img src="{!! asset($postView->img_path) !!}" alt="" style="width: 600px">

                        <h2 class="post-title">
                           {{$viewPost['title']}}
                        </h2>
                        <h3 class="post-subtitle">
                            {{$viewPost['sub_title']}}
                        </h3>
                        <br>
                        <h3 class="post-meta">
                            {{$viewPost['summary']}}
                        </h3>
                    <p class="post-meta"> {{$viewPost['details']}}</p>
                </div>
                <hr>
            </div>
        </div>
    </div>

    <hr>

@endsection

