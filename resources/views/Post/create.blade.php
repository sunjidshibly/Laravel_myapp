@extends('Layouts.default')   



@section('Layouts.content')

    <div id="page-wrapper">
        <div class="row">
        <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <h1 class="page-header">Add Article</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        @foreach ($errors->all() as $error)
            <p class="alert alert-danger">{{ $error }}</p>
        @endforeach
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                        <div class="col-lg-2"></div>
                            <div class="col-lg-8">
                                {!! Form::open(['route' => 'myPost.store', 'files'=> true]) !!}
                                 {!! Form::hidden('user_id', Auth::user()->id )!!}
                                    <div class="form-group">
                                        {!! Form::label('title', 'Title') !!}
                                        {!! Form::text('title', null, ['class' => 'form-control','required','autofocus']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('sub_title', 'Sub-Title') !!}
                                        {!! Form::text('sub_title', null, ['class' => 'form-control']) !!}
                                    </div>
                                     <div class="form-group">
                                        {!! Form::label('img_path', 'Image') !!}
                                        {!! Form::file('img_path') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('img_caption', 'Image Caption') !!}
                                        {!! Form::text('img_caption', null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('summary', 'Summery') !!}
                                        {!! Form::textarea('summary', null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('details', 'Details') !!}
                                        {!! Form::textarea('details', null, ['class' => 'form-control']) !!}
                                    </div>
                                {!! Form::submit('Submit', array('class'=>'btn btn-primary')) !!}
                                {!! Form::reset('Reset', array('class'=>'btn btn-warning')) !!}

                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>




@endsection
