@extends('Layouts.default')   



@section('Layouts.content')
        
    <!-- Post Content -->
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                 <center> <div > 
                   <a class="btn btn-info" href="imageUpload/create" role="button">New Image</a>
                </div> </center>
                
                    <br>
                 <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <tr>
                                    <th>Serial no</th>
                                    <th>Image</th>
                                    <th>Image Path</th>
                                    <th>Extension</th>
                                    <th>Image Caption</th>
                                    <th>Action</th>
                                </tr>
                        
                    </tr>
                    </thead>
                     <tbody>
                                @foreach($images as $image)
                                <tr class="gradeA">
                                    <td>{!! $image->id !!}</td>
                                    <td><img src="{!! asset($image->img_path) !!}" alt="" style="width: 100px"></td>
                                    <td>{!! $image->img_path !!}</td>
                                    <td>{!! $image->extension !!}</td>
                                    <td>{!! $image->img_caption !!}</td>
                                    <td>
                                        {!! Form::open(array('method'=>'DELETE', 'route'=>array('imageUpload.destroy',$image->id)))!!}
                                        {!! Form::submit('Delete', array('class'=>'btn btn-danger btn-sm','onclick' => 'return confirm("Are you sure want to Delete?");'))!!}
                                        {!! Form::close()!!}
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>

                 
                </table>

                </div>
            </div>
        </div>
    </article>

    <hr>

    @endsection