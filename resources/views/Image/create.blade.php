@extends('Layouts.default')   



@section('Layouts.content')	<div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                {!! Form::open(['route' => 'imageUpload.store', 'files'=> true]) !!}
                                    <div class="form-group">
                                        {!! Form::label('img_path', 'Image') !!}
                                        {!! Form::file('img_path') !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('img_caption', 'Image Caption') !!}
                                        {!! Form::text('img_caption', null, ['class' => 'form-control']) !!}
                                    </div>
                                {!! Form::submit('Submit', array('class'=>'btn btn-primary')) !!}
                                {!! Form::reset('Reset', array('class'=>'btn btn-warning')) !!}

                                {!! Form::close() !!}
                            </div>
                            <div class="col-lg-3"></div>





@endsection