<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
       protected $fillable = [
        'user_id','title', 'sub_title', 'summary', 'details'
    ];
    protected $delete=['deleted_at'];

     public function users()
    {
        return $this->belongsTo('App\User');
    }
     public function images()
    {
        return $this->belongsToMany('App\Image');
    }
}
