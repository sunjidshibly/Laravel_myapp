<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;
use App\User;
use App\Image;
use Auth;

class postController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

        public function __construct()
    {
        $this->middleware('auth', ['only' => ['index','create', 'edit', 'store','update','destroy','ImageUpload']]);
        $this->middleware('user_handle', ['only' => ['index_admin']]);
    }
    
    public function index()
    {
    $background='post-bg.jpg';
    $sectionTilte='Man must explore, and this is exploration at its greatest';
    $pageTitle = "Post";
    $subtitle='Problems look mighty small from 150 miles up';

    $id = Auth::user()->id;
    $posts = Post::orderBy('created_at', 'desc')->get()->where('user_id',$id) ;

    return view('Post.post')->with(compact('sectionTilte','subtitle','background','pageTitle','posts'));
    }

    public function index_common()
    {
   $background='home-bg.jpg';
    $sectionTilte='Clean Blog';
    $pageTitle = "Clean Blog : Welcome";
    $subtitle='A Clean Blog Theme by Start Bootstrap';

    $posts = Post::with('users')->orderBy('created_at', 'desc')->get();
// dd($posts);

    return view('Article.index')->with(compact('sectionTilte','subtitle','background','pageTitle','posts'));
    }

    public function index_admin()
    {
   $background='home-bg.jpg';
    $sectionTilte='Clean Blog';
    $pageTitle = "Clean Blog : Welcome";
    $subtitle='A Clean Blog Theme by Start Bootstrap';

    $posts = Post::with('users')->orderBy('created_at', 'desc')->get();
// dd($posts);

    return view('Admin.deleteIndex')->with(compact('sectionTilte','subtitle','background','pageTitle','posts'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    $background='post-bg.jpg';
    $sectionTilte='';
    $pageTitle = "Post";
    $subtitle='';

    return view('Post.create')->with(compact('sectionTilte','subtitle','background','pageTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        
        $post=Post::create($request->all());

        $img=$this->ImageUpload($request->img_path);
        $imgData = new Image();
        $imgData->img_path = $img;
        $imgData->img_caption = $request->img_caption;
        $imgData->extension =$request->img_path->getClientOriginalExtension();
        $imgData->save();
        $lastImageId = $imgData->id;
        $post->images()->attach($lastImageId);

       return redirect('/myPost') ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $background='post-bg.jpg';
        $sectionTilte='';
        $pageTitle = "";
        $subtitle='';
        $viewPost = Post::with('images')->find( $id );

//dd($viewPost);
        return view('Post.view',compact('viewPost','sectionTilte','subtitle','background','pageTitle'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //  dd($id);
       $background='post-bg.jpg';
        $sectionTilte='';
        $pageTitle = "";
        $subtitle='';
        $editPost = Post::find( $id );
        return view('Post.update',compact('editPost','sectionTilte','subtitle','background','pageTitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editPost = Post::find( $id )->update($request->all());
        return redirect('/myPost');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deletePost = Post::find( $id );
        $deletePost->delete();
        return redirect('/myPost') ;
    }

            public function ImageUpload($image)
    {
        $extension =$image->getClientOriginalExtension();//get image extension only
        $extensionArray = array("jpg", "jpeg", "png", "gif");

        $path="img/uploaded";
        if(in_array($extension, $extensionArray)){
            $imageOriginalName=$image->getClientOriginalName();//get image full name
            $basename = substr($imageOriginalName, 0 , strrpos($imageOriginalName, "."));//get image name without extension
            $imageName=$basename.date("YmdHis").'.'.$extension;//make new name

            $imageMoved=$image->move($path, $imageName);
            if($imageMoved)
            {
                $imagePath=$path.'/'.$imageName;
                return $imagePath;
            }

        }
        else{
            return "Please insert image";
        }
    }
}
