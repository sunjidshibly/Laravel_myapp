<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Category;

class categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

        public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user_handle');
    }
    
    public function index()
    {
 $background='contact-bg.jpg';
    $sectionTilte='Create Catetagory';
    $pageTitle = "Clean Blog : Create Catetagory";
    $subtitle='Need Catagories? You can create here.';

    $categories = Category::orderBy('created_at', 'desc')->get();
    

 return view('Category.category')->with(compact('sectionTilte','subtitle','background','pageTitle','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
 //    $background='contact-bg.jpg';
 //    $sectionTilte='Contact Me';
 //    $pageTitle = "Clean Blog : Contact me";
 //    $subtitle='Have questions? I have answers (maybe).';

 // return view('Article.contact')->with(compact('sectionTilte','subtitle','background','pageTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        Category::create($request->all());
        return redirect('/category') ;

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $background='contact-bg.jpg';
        $sectionTilte='';
        $pageTitle = "";
        $subtitle='';
        $editCategory = Category::find( $id );
        return view('category.edit',compact('editCategory','sectionTilte','subtitle','background','pageTitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $editCategory = Category::find( $id )->update($request->all());
        return redirect('/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $deleteCategory = Category::find( $id );
        $deleteCategory->delete();
        return redirect('/category') ;
    }
}
