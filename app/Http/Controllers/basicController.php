<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class basicController extends Controller
{
     public function home()
     {
    $background='home-bg.jpg';
    $sectionTilte='Clean Blog';
    $pageTitle = "Clean Blog : Welcome";
    $subtitle='A Clean Blog Theme by Start Bootstrap';
    return view('Article.index')->with(compact('sectionTilte','subtitle','background','pageTitle'));
} 

    public function profile()
     {
    $profile=Auth::user();
    //dd($profile);
    $background='home-bg.jpg';
	$sectionTilte=Auth::user()->name;
	$pageTitle = "Clean Blog : User's Profile";
	$subtitle='See your Profile';
    return view('Profile.index')->with(compact('sectionTilte','subtitle','background','pageTitle','profile'));
}



 public function about()
    {
    $background='about-bg.jpg';
	$sectionTilte='About Me';
	$pageTitle = "Clean Blog : About";
	$subtitle='This is what I do ';

    return view('Article.about')->with(compact('sectionTilte','subtitle','background','pageTitle'));
    }


}
