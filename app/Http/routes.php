<?php




Route::get('/', 'postController@index_common');
Route::get('/deleteIndex', 'postController@index_admin');
Route::get('/home', 'postController@index_common');
Route::get('/profile', 'basicController@profile');
Route::get('/about', 'basicController@about');
Route::resource('/category', 'categoryController');
Route::resource('/myPost', 'postController');
Route::resource('/imageUpload', 'imageuploadController');
Route::auth();


