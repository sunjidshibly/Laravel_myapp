<?php



$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'left' => $faker->boolean,
        'right' => $faker->boolean,
        'parent_id' => $faker->year,
    ];
});

$factory->define(App\Post::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'sub_title' => $faker->sentence,
        'summary' => $faker->paragraph,
        'details' => $faker->paragraph,
    ];
});
